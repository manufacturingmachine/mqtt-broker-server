import {Context} from "koa";
import Router = require("koa-router");
import {Database} from "../database/databse-structure";
import {sqlConnection} from "../database/mysql-connect";

const enum AggregationType {
    SyncedValues = 'SyncedValues',
    SensorsPerBatch = 'SensorsPerBatch',
    SensorsPerTool = 'SensorsPerTool',
    FailureRatePerBarch = 'FailureRatePerBarch',
    FailureRatePerTool = 'FailureRatePerTool',
    GoodVsFailed = 'GoodVsFailed',
}

export class PublicRouter extends Router {

    constructor(args?: any) {
        super(args);

        this.get('/aggregate/:aggregationType', async (ctx: Context) => {
            const aggregationType = ctx.params.aggregationType;
            const mySqlDbConnection = sqlConnection.get(Database.MyDb);
            // switch (aggregationType) {
            // case AggregationType.SensorsPerBatch: {
            const [results, fields] = await mySqlDbConnection.client.query(
                `SELECT * FROM ${aggregationType}`
            );
            ctx.body = results;
            // }
            // }
            ctx.status = 200;
            // ctx.body
        });
    }
}
