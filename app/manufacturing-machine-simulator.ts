/*
Requirement 1:
Generate objects that look like this:
{
    partId: string, //10 digit string
    batchId: string, //7 digit string, 200 parts per batch
    timestamp: string/UTC date, //time when the measurements were taken
    toolId: integer, // will be 1 if last character in partId is odd, otherwise is 2
    deviation: float, //parameter 1, generated using Math.random
    rotation: float, //parameter 2, generated using Math.random,
    errorCode: integer //can have these values: 1 if deviation is outside limits, 2 if rotation is outside limits, 3 if both are outside limits, otherwise 0
}

Additional data:
upperLimit: float, //uppermost value the deviation and rotation should be to be considered OK
lowerLimit: float, //lowermost value the deviation and rotation should be to be considered OK

proposed values:
upperLimit = 20
lowerLimit = 10

parameter limits for Math.random
7 - 22
 */
import fs = require('fs');
import {sqlConnection} from "./database/mysql-connect";
import {Database} from "./database/databse-structure";

const totalParts = 20000
    , upperLimit = 20
    , lowerLimit = 10
    , upperRandom = 22
    , lowerRandom = 7
;
const lastDay = new Date(Date.now());
lastDay.setDate(lastDay.getDate() - 1);
export const referenceTimestamp = (new Date('2019-01-19T17:28:58.000Z')).getTime();
let i = 0
    , batchId: string
    , timestamp = referenceTimestamp
    , deviation: number
    , rotation: number
    , errorCode: number
    , unsafeDeviation: boolean
    , unsafeRotation: boolean
;
export let currentPartId = 1000000000;

interface Parts {
    partId: number,
    batchId: string
    timestamp: number,
    // nextTimestamp: number,
    toolId: number
    deviation: number
    rotation: number
    errorCode: number
}

//generate the data
export async function generateData() {
    const mySqlDbConnection = sqlConnection.get(Database.MyDb);
    let values: any = [];
    let damagedParts = [];

    for (; i < totalParts; i++) {
        if (i % 200 === 0) {
            batchId = Math.floor(1000000 + Math.random() * 9000000).toString();
        }
        deviation = (Math.random() * (upperRandom - lowerRandom) + lowerRandom);
        rotation = (Math.random() * (upperRandom - lowerRandom) + lowerRandom);

        unsafeDeviation = deviation > upperLimit || deviation < lowerLimit;
        unsafeRotation = rotation > upperLimit || rotation < lowerLimit;

        errorCode = 0;
        if (unsafeRotation && unsafeDeviation) {
            errorCode = 3;
        } else if (unsafeRotation) {
            errorCode = 2;
        } else if (unsafeDeviation) {
            errorCode = 1;
        }
        // The machine simulator should be able to generate data with a frequency of 3 parts per minute. => 60000 / 3 = 20000
        timestamp += 20000;

        const g: Parts = {
            partId: ++currentPartId,
            batchId: batchId,
            timestamp: timestamp,
            // nextTimestamp: timestamp + 20000,
            toolId: currentPartId % 2 ? 1 : 2,
            deviation: deviation,
            rotation: rotation,
            errorCode: errorCode
        };
        values.push([
            g.partId.toString(),
            g.batchId.toString(),
            (new Date(g.timestamp)).toISOString()/*, (new Date(g.nextTimestamp)).toISOString()*/,
            g.toolId, g.deviation, g.rotation, upperLimit, lowerLimit
        ]);

        if (errorCode !== 0) {
            damagedParts.push([g.partId.toString(), errorCode])
        }

        if (i % 5000 === (5000 - 1)) {
            const [results, fields] = await mySqlDbConnection.client.query(
                'INSERT INTO Components VALUES ?',
                [values]
            );
            values = [];
            console.log('The solution is: ', results);
            await mySqlDbConnection.client.query(
                'INSERT INTO DamagedComponents VALUES ?',
                [damagedParts]
            );
            damagedParts = [];
        }

    }

    // fs.writeFile("./test.json", JSON.stringify(parts), function(err) {
    //     if(err) {
    //         return console.log(err);
    //     }
    //     console.log("The file was saved!");
    // });
}