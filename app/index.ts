import {Server as HttpServer} from "http";
import Application = require("koa");
import {errorHandler} from "./utils/error-handler";
import {PublicRouter} from "./routes/public-router";

const cors: any = require('koa-cors');
const convert: any = require('koa-convert');
const koaBetterBody = require('koa-better-body');

export class KoaServer {
    private app: Application;
    public listeningServer: HttpServer;

    constructor() {
        console.log('Create KOA Server');
        this.app = new Application();

        this.configMiddlewares();
        this.configRoutes();
    }

    private configMiddlewares() {
        console.log('Config KOA Server');
        this.app.use(convert(koaBetterBody({
            files: true,
            jsonLimit: '50mb'
        })));
        this.app.use(convert(cors()));
        this.app.use(errorHandler());
    }

    private configRoutes() {
        const privateRouter = new PublicRouter();
        this.app
            .use(privateRouter.routes())
            .use(privateRouter.allowedMethods());
    }


    async start() {
        const serverPort = 4000;
        this.listeningServer = this.app
            .listen(serverPort, (error?: any) => {
                if (error) {
                    console.log('Error while trying to open server.', error);
                    return;
                }
                console.log('🚀  Server started on port', serverPort);
            });
        return this.listeningServer;
    }
}