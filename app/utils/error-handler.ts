import {IRouterContext} from "koa-router";

export function errorHandler() {
    return async(ctx: IRouterContext, next: any) => {
        try {
            await next();
        } catch (err) {
            console.error(err);
            // if (err instanceof MyError) {
            //     ctx.body = err.response;
            // } else {
            //     ctx.body = err.message;
            // }
            ctx.status = err.status || 500;
            ctx.app.emit('error', err, ctx);
        }
    }
}