/*
Requirement 1:
Generate objects that look like this:
{
    partId: string, //10 digit string
    batchId: string, //7 digit string, 200 parts per batch
    timestamp: string/UTC date, //time when the measurements were taken
    toolId: integer, // will be 1 if last character in partId is odd, otherwise is 2
    deviation: float, //parameter 1, generated using Math.random
    rotation: float, //parameter 2, generated using Math.random,
    errorCode: integer //can have these values: 1 if deviation is outside limits, 2 if rotation is outside limits, 3 if both are outside limits, otherwise 0
}

Additional data:
upperLimit: float, //uppermost value the deviation and rotation should be to be considered OK
lowerLimit: float, //lowermost value the deviation and rotation should be to be considered OK

proposed values:
upperLimit = 20
lowerLimit = 10

parameter limits for Math.random
7 - 22
 */
import {sqlConnection} from "./database/mysql-connect";
import {Database} from "./database/databse-structure";
import {inspect} from "util";

const totalParts = 20000
    , upperLimit = 20
    , lowerLimit = 10
    , upperRandom = 22
    , lowerRandom = 7
;
let i = 0
    , batchId: string
    , deviation: number
    , rotation: number
    , errorCode: number
    , unsafeDeviation: boolean
    , unsafeRotation: boolean
;
export let currentPartId = 1000000000;

interface Parts {
    partId: number,
    batchId: string
    timestamp: number,
    toolId: number
    deviation: number
    rotation: number
    errorCode: number
}

//generate the data
export async function generateData(lastPartId?: number) {
    if (lastPartId != null)
        currentPartId = lastPartId;
    const mySqlDbConnection = sqlConnection.get(Database.MyDb);
    for (; i < totalParts; i++) {
        ++currentPartId;
        if (i % 200 === 0) {
            batchId = Math.floor(1000000 + Math.random() * 9000000).toString();
        }
        deviation = (Math.random() * (upperRandom - lowerRandom) + lowerRandom);
        rotation = (Math.random() * (upperRandom - lowerRandom) + lowerRandom);

        unsafeDeviation = deviation > upperLimit || deviation < lowerLimit;
        unsafeRotation = rotation > upperLimit || rotation < lowerLimit;

        errorCode = 0;
        if (unsafeRotation && unsafeDeviation) {
            errorCode = 3;
        } else if (unsafeRotation) {
            errorCode = 2;
        } else if (unsafeDeviation) {
            errorCode = 1;
        }
        // The machine simulator should be able to generate data with a frequency of 3 parts per minute. => 60000 / 3 = 20000

        const g: Parts = {
            partId: currentPartId,
            batchId: batchId,
            timestamp: Date.now(),
            toolId: currentPartId % 2 ? 1 : 2,
            deviation: deviation,
            rotation: rotation,
            errorCode: errorCode
        };
        console.log(inspect(g, null, null, true));

        if (errorCode !== 0) {
            await mySqlDbConnection.client.query(
                'INSERT INTO DamagedComponents VALUES ?',
                [[[g.partId.toString(), errorCode]]]
            );
        }

        const [results, fields] = await mySqlDbConnection.client.query(
            'INSERT INTO Components VALUES ?',
            [[[
                g.partId.toString(),
                g.batchId.toString(),
                (new Date(g.timestamp)).toISOString(),
                g.toolId,
                g.deviation,
                g.rotation,
                upperLimit,
                lowerLimit
            ]]]
        );
        console.log('The solution is: ', results);
        await delay(20000);
    }
}

function delay(ms: number) {
    return new Promise(function(resolve) {
        setTimeout(resolve.bind(null), ms)
    });
}
