export const enum Database {
    // MyDb = 'store'
    // MyDb = 'bigdataproject'
    MyDb = 'team6'
}

export interface DatabaseConfig {
    database: Database,
    host: string,
    port: number,
    user: string,
    password: string,
    connectionLimit: number,
    stream?: any
}
