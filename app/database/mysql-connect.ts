import {ConnectConfig} from "ssh2";
import {Connection} from "promise-mysql";
import {Database, DatabaseConfig} from "./databse-structure";
import {mySqlSSHClose, mySqlSSHConnect} from "./mysql-ssh";

const mysql = require('mysql2');

export const sqlConnection: Map<string, {client: Connection, close: Function}> = new Map();

export async function connectToMsSqlServer(mySqlConfig: DatabaseConfig, sshConfig?: ConnectConfig) {
    console.log(`Connecting to database ${mySqlConfig.database}.`);
    try {
        if (sshConfig) {
            const client = await mySqlSSHConnect(sshConfig, mySqlConfig);
            sqlConnection.set(Database.MyDb, {client, close: () => mySqlSSHClose()});
        } else {
            const client = await mysql.createConnection(mySqlConfig);
            sqlConnection.set(Database.MyDb, {client, close: () => {}});
        }
        console.log(`Connection to database: ${mySqlConfig.database} on port ${mySqlConfig.port} at ${mySqlConfig.host} opened`);
    } catch (e) {
        console.error(`Unable to connect to database: ${mySqlConfig.database} on port ${mySqlConfig.port} at ${mySqlConfig.host} - ${e}`);
        throw e;
    }
}

// const pool = sqlPool.get(databaseName);
// pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//     if (error) throw error;
//     console.log('The solution is: ', results[0].solution);
// });
