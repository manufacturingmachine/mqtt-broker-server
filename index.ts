import {inspect} from "util";
import {connectToMsSqlServer, sqlConnection} from "./app/database/mysql-connect";
import {Database} from "./app/database/databse-structure";

const mosca = require('mosca');

const enum MqttTopic {
    XDK110 = 'BCDS/XDK110/sensors/data',
    ManufacturingMachine = 'ManufacturingMachine'
}

const enum SensorType {
    Humidity = 'humidity',
    Pressure = 'pressure',
    Temperature = 'temperature',
    Light = 'light',
    Noise = 'noise',
}

interface SensorData {
    title: string,
    data: {
        type: SensorType,
        value: number
    }[]
}

const ascoltatore = {
    //using ascoltatore
    type: 'mongo',
    url: 'mongodb://root:root@mongo:27017/mqtt',
    // url: 'mongodb://localhost:27017/mqtt',
    pubsubCollection: 'ascoltatori',
    mongo: {}
};

const moscaSettings = {
    port: 1883,
    backend: ascoltatore,
    persistence: {
        factory: mosca.persistence.Mongo,
        url: 'mongodb://root:root@mongo:27017/mqtt'
        // url: 'mongodb://localhost:27017/mqtt'
    },
    http: {
        port: 3000,
        bundle: true,
        static: './'
    }
};

const server = new mosca.Server(moscaSettings);
server.on('ready', setup);

server.on('clientConnected', function (client: any) {
    console.log('client connected', client.id);
});

let currentPartId: string;

// let sensorsTimestamp = referenceTimestamp;
// fired when a message is received
server.on('published', async function (packet: any, client: any) {

    const topic = packet.topic;
    console.log('topic', topic);
    if (topic === MqttTopic.ManufacturingMachine) {
        currentPartId = packet.payload.toString();
        console.log('currentPartId', currentPartId);
    } else if (topic === MqttTopic.XDK110) {
        const payload = packet.payload.toString();
        try {
            let jsonPayload: SensorData = JSON.parse(payload);
            const mySqlDbConnection = sqlConnection.get(Database.MyDb);
            console.log(`INSERT INTO SyncSensorsValues (title, ${jsonPayload.data.map(d => d.type).join(', ')}, timestamp)`);
            const [results, fields] = await mySqlDbConnection.client.query(
                `INSERT INTO SyncSensorsValues (part_id, title, ${jsonPayload.data.map(d => d.type).join(', ')}, timestamp) VALUES ?`,
                [[[currentPartId.toString(), jsonPayload.title, ...jsonPayload.data.map(d => d.value), (new Date()).toISOString()]]]
            );
            console.log('Published', inspect(jsonPayload, null, null, true));
            console.log(results);
            // sensorsTimestamp += 20000;
        } catch (e) {
            console.log(e);
            console.log('Published', payload);
        }
    }

});

// fired when the mqtt server is ready
async function setup() {
    await connectToMsSqlServer(
        {
            database: Database.MyDb,
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: 'cloudera',
            connectionLimit: 10,
        },
        {
            host: '10.111.0.250',
            port: 22,
            username: 'team6',
            password: 'team6_b1i2'
        }
    );

    const mySqlDbConnection = sqlConnection.get(Database.MyDb);
    const [results, fields] = await mySqlDbConnection.client.query('SELECT MAX(part_ID) as currentPartId FROM Components');
    currentPartId = results[0].currentPartId;
    // console.log('START Manufacturing machine simulator');
    // const SIMULATOR = 'Manufacturing machine simulator';
    // console.time(SIMULATOR);
    // generateData(results.length > 0 ? Number.parseInt(results[0].currentPartId) : null)
    //     .then(() => {
    //         console.log('Manufacturing machine simulator finish to generate data');
    //         console.timeEnd(SIMULATOR);
    //     })
    //     .catch(err => console.error(err));

    // console.log('The solution is: ', results);
    console.log('Mosca server is up and running');

    // Sending data from mosca to clients
    const message = () => ({
        topic: MqttTopic.XDK110,
        payload: JSON.stringify({
            title: "Environmental Data",
            data: [
                {type: SensorType.Humidity, value: Math.random() * 10},
                {type: SensorType.Pressure, value: Math.random() * 10},
                {type: SensorType.Light, value: Math.random() * 10},
                {type: SensorType.Noise, value: Math.random() * 10},
                {type: SensorType.Temperature, value: Math.random() * 10}
            ],
            timestamp: Date.now()
        }), // or a Buffer
        qos: 0, // 0, 1, or 2
        retain: false // or true
    });
    setInterval(() => {
        server.publish(message(), function () {
            console.log('done!');
        });
    }, 1000);

    // await (new KoaServer()).start();
}
