# Use an official Node.js runtime as a parent image
FROM node:10.1

# Define environment variable
ENV HOME=/app

# Copy the current directory contents into the container at /app/server
COPY . $HOME/mqtt-broker-server

# Set the working directory to /app
WORKDIR $HOME/mqtt-broker-server

RUN npm i -g typescript

# Install packages
RUN npm install

## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

## Launch the wait tool and then your application
## Run `npm start` when the container launches
CMD /wait && tsc && npm start

